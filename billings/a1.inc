<?php

/**
 * Billing info
 */
function a1_info() {
  $name = t('A1 - agregator');
  return array(
    'billing' => t('a1'),
    'name' => $name,
    'settings_description' => t('Settings @name.', array('@name' => $name)),
  );
}

/**
 * Billing sms format
 */
function a1_sms_format() {
  return array(
    'sms_id' => 'smsid',
    'key' => 'skey',
    'operator_id' => 'operator_id',
    'number' => 'num',
    'msg' => 'msg',
    'user_id' => 'user_id',
  );
}

/**
 * Billing response format
 */
function a1_response_format($sms) {
  return array('smsid: '. $sms->sms_id, 'status: reply', '', $sms->response);
}